// OOP Short Course Capstone

class Customer {
	constructor(email) {
		this.email = email;
		this.cart = new Cart ();
		this.orders = [];
	}

	checkOut () {

		if (this.cart.contents.length == 0) {
			console.error("Cart is Empty")
			return this;
		} else {
			this.cart.computeTotal();
			this.orders.push({
				products: this.cart.contents,
				totalAmount: this.cart.totalAmount
			});
			this.cart = [];
			return this;
		}	
	}
}


class Cart {
	constructor () {
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart (product, qty) {
		this.contents.push({
			product: product,
			quantity: qty
		})

		return this
	}

	showCartContents() {
		return this.contents;
	}

	updateProductQuantity (prodName, newQty) {

		for (let i=0; i < this.contents.length; i++) {
 				if (this.contents[i].product.name === prodName){
 					this.contents[i].quantity = newQty;
 				}
 			}

		return this;
	}

	clearCartContents () {
		this.contents = [];
		return this;
	}

	computeTotal () {
		let total = 0;
		this.contents.forEach(content => total = total + (content.product.price*content.quantity));
		this.totalAmount = Math.round((total + Number.EPSILON) * 100) / 100
		return this;	
	}
}

class Product {
	constructor (name, price) {
		this.name = name;

		if (typeof price === "number") {
			if (price <= 0) {
				console.error("Please use value greater than 0 for the price.")
				return this;
			} else {
				this.price = Math.round((price + Number.EPSILON) * 100) / 100;
				return this;
			}
		} else {
			console.error("Please use a numeric value for the price.")
			return this;
		}

		this.isActive = true;
	}

	archive () {

		if (this.isActive === true) {
			this.isActive = false;
			return this;
		} else {
			console.error("Product is already inactive.")
			return this;
		}	
	}

	updatePrice (newPrice) {

		if (typeof newPrice === "number") {
			if (newPrice <= 0) {
				console.error("Please use value greater than 0 for the new price.")
				return this;
			} else {
				this.price = Math.round((newPrice + Number.EPSILON) * 100) / 100;
				return this;
			}
		} else {
			console.error("Please use a numeric value for the new price.")
			return this;
		}
	}
}


// Customer Creation
const john = new Customer ("john@mail.com");


// Product Creation
const prodA = new Product("soap", 9.99);
const prodB = new Product("shampoo", 8.99);
const prodC = new Product("toothpaste", 6.99);


/*

prodA.updatePrice(12.99)
prodA.archive()
john.cart.addToCart(prodA, 3)
john.cart.showCartContents()
john.cart.updateProductQuantity("soap", 6)
john.cart.clearCartContents()
john.cart.computeTotal()
john.checkOut()

*/